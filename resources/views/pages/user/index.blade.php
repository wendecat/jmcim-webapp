@extends('adminlte::page')

@section('title','JMCIM Webapp')

@section('content_header')
    <h1>JMCIM<small>User</small></h1>
@endsection

@section('css')
    <link rel="stylesheet"
          href="{{ asset('css/app.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/fonts/font_roboto/roboto.css') }}">

@endsection

@section('content')
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    @include('layouts/error_box')

                    <a href="{{ route('user.create') }}" title="Add New" class="btn btn-primary btn-xs">
                        <i class="glyphicon glyphicon-plus"></i>Add New
                    </a>
                    <br></br>

                    <div class="table-responsive">
                    <table class="table table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<script src="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var dataTable = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: '{{ route("user.index") }}',
                columnDefs: [
                    {
                        'targets': 0,
                        'data': 'id',
                        'name': 'id',
                    },
                    {
                        'targets': 1,
                        'data': 'name',
                        'name': 'name',
                    },
                    {
                        'targets': 2,
                        'data': 'username',
                        'name': 'username',
                    },
                    {
                        'targets': 3,
                        'data': 'email',
                        'name': 'email',
                    },
                    {
                        'targets': 4,
                        'searchable': false,
                        'sortable': false,
                        'render': function(data,type,row){
                            var url = '{{ route('user.edit',':id') }}'.replace(':id',row.id);

                           		return [
								'<a href="' + url + '" title="Edit" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>' + " " +
								'<button data-id="' + row.id + '" type="button" title="Delete" class="btn btn-danger btn-xs delete-btn" ><i class="fa fa-trash"></i> Delete</button>'
								
							];
                        },
                    }
                ]
            });

        $(document).on("click",".delete-btn", function(){
            var self = this;
            var id = $(this).attr('data-id');
            var url = '{{ route('user.destroy',':id') }}'.replace(':id',id);

            dhtmlx.confirm({
                title: "Confirm Deletion",
                type: "confirm-warning",
                text: "Do you really want to delete this record?",
                callback: function(res){
                    if(!res){
                        return;
                    }
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response){
                            dhtmlx.alert({
                                title: 'Record Deleted',
                                text: 'The record has been successfully deleted',
                                callback: function() {
                                    dataTable.draw();
                                }
                            });
                        },
                        error: function(err){
                            alert("error");
                        }
                    });
                }
            });
        });
}); // end jQuery

        /**
        *  Delete record
        */
    
    </script>
@endsection