@extends('adminlte::page')

@section('title','JMCIM Webapp')

@section('content_header')
    <h1>JMCIM <small>Edit User</small></h1>
@endsection

@section('css')
<link rel="stylesheet"
          href="{{ asset('css/app.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/fonts/font_roboto/roboto.css') }}">

@endsection

@section('content')    
<div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    @include('layouts/error_box')
                    {{ Form::model($user, ['route' => ['user.update',$user->id], 'method' => 'PUT', 'class' => 'form-horizontal'])}}
                    {{ csrf_field() }}

                    @include('pages.user.partials.forms')
                    <div class="form-group">
                        <div class="col-xs-1 col-xs-offset-2">
                            <button type="submit" class="btn btn-primary" placeholder"Submit"><i class="glyphicon glyphicon-check"></i> Save</button>
                        </div>
                        <div class="col-xs-1">
                            <a href="{{ route('user.index') }}" title="Cancel" class="btn btn-primary cancel_btn">
                            <i class="glyphicon glyphicon-remove-circle"></i> Cancel
                            </a>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.js') }}"></script>
@endsection