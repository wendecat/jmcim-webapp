<div class="form-group">
    <label for="name" class="col-xs-2 control-label">Name:</label>
    <div class="col-xs-5">
    <input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="name">
    </div>
</div>
<div class="form-group">
    <label for="username" class="col-xs-2 control-label">Username:</label>
    <div class="col-xs-5">
    <input type="text" name="username" class="form-control" value="{{ $user->username }}" placeholder="username">
    </div>
</div>
<div class="form-group">
    <label for="email" class="col-xs-2 control-label">Email:</label>
    <div class="col-xs-5">
    <input type="text" name="email" class="form-control" value="{{ $user->email }}" placeholder="email">
    </div>
</div>
<div class="form-group">
    <label for="password" class="col-xs-2 control-label">Password:</label>
    <div class="col-xs-5">
    <input type="password" name="password" class="form-control" value="{{ $user->password }}" placeholder="password">
    </div>
</div>