@extends('adminlte::page')

@section('title','JMCIM Webapp')

@section('content_header')
    <h1>JMCIM<small>Member</small></h1>
@endsection

@section('css')
    <link rel="stylesheet"
          href="{{ asset('css/app.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.css') }}">

    <link rel="stylesheet"
          href="{{ asset('vendor/dhtmlxSuite/codebase/fonts/font_roboto/roboto.css') }}">

@endsection

@section('content')
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    @include('layouts/error_box')

                    <a href="{{ route('member.create') }}" title="Add New" class="btn btn-primary btn-xs">
                        <i class="glyphicon glyphicon-plus"></i>Add New
                    </a>
                    <br></br>
                    <a href="#" data-action="{{url('testimony/5')}}" data-toggle="laramodal" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-th-list"></i> Testimonies</a>
                    <br></br>

                    <div class="table-responsive">
                    <table class="table table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Member Type</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Date Saved</th>
                                <!-- <th>Address</th> -->
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Birth Date</th>
                                <!-- <th>Occupation</th>
                                <th>Special Skill</th>
                                <th>Vices</th>
                                <th>Diseases</th> -->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.js') }}"></script>
    <script src="{{ asset('vendor/laramodal/laramodal.js')}}" ></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var dataTable = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                deferRender: true,
                ajax: '{{ route("member.index") }}',
                columnDefs: [
                    {
                        'targets': 0,
                        'data': 'id',
                        'name': 'id',
                    },
                    {
                        'targets': 1,
                        'data': 'type.name',
                        'name': 'type.name',
                    },
                    {
                        'targets': 2,
                        'data': 'name',
                        'name': 'name',
                    },
                    {
                        'targets': 3,
                        'data': 'gender',
                        'name': 'gender',
                    },
                    {
                        'targets': 4,
                        'data': 'date_saved',
                        'name': 'date_saved',
                    },
                    // {
                    //     'targets': 5,
                    //     'data': 'address',
                    //     'name': 'address',
                    // },
                    {
                        'targets': 5,
                        'data': 'contact',
                        'name': 'contact',
                    },
                    {
                        'targets': 6,
                        'data': 'email',
                        'name': 'email',
                    },
                    {
                        'targets': 7,
                        'data': 'birth_date',
                        'name': 'birth_date',
                    },
                    // {
                    //     'targets': 9,
                    //     'data': 'occupation',
                    //     'name': 'occupation',
                    // },
                    // {
                    //     'targets': 10,
                    //     'data': 'special_skill',
                    //     'name': 'special_skill',
                    // },
                    // {
                    //     'targets': 11,
                    //     'data': 'vices',
                    //     'name': 'vices',
                    // },
                    // {
                    //     'targets': 12,
                    //     'data': 'diseases',
                    //     'name': 'diseases',
                    // },
                    {
                        'targets': 8,
                        'searchable': false,
                        'sortable': false,
                        'render': function(data,type,row){
                            var edit = '{{ route('member.edit',':id') }}'.replace(':id',row.id);
                            var show = '{{ route('testimony.show',':id') }}'.replace(':id',row.id);
                            
                           		return [
								'<a href="' + edit + '" title="Edit" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>' + " " +
                                '<button data-id="' + row.id + '" type="button" title="Delete" class="btn btn-danger btn-xs delete-btn" ><i class="fa fa-trash"></i> Delete</button>' + " " +
                                '<a href="#" data-action="' + show + '" data-toggle="laramodal" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-th-list"></i> Testimonies</a>'
								
							];
                        },
                    }
                ]
            });

        $(document).on("click",".delete-btn", function(){
            var self = this;
            var id = $(this).attr('data-id');
            var url = '{{ route('member.destroy',':id') }}'.replace(':id',id);
            
            dhtmlx.confirm({
                title: "Confirm Deletion",
                type: "confirm-warning",
                text: "Do you really want to delete this record?",
                callback: function(res){
                    if(!res){
                        return;
                    }
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response){
                            dhtmlx.alert({
                                title: 'Record Deleted',
                                text: 'The record has been successfully deleted',
                                callback: function() {
                                    dataTable.draw();
                                }
                            });
                        },
                        error: function(err){
                            alert("error");
                        }
                    });
                }
            });
        });
}); // end jQuery
    
    </script>
@endsection