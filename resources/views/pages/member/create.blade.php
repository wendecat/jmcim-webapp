@extends('adminlte::page')

@section('title','JMCIM Webapp')

@section('content_header')
<h1>JMCIM <small>Add Member</small></h1>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    @include('layouts/error_box')
                    {{ Form::model($member, ['route' => ['member.store',$member->id], 'method' => 'POST', 'class' => 'form-horizontal'])}}
                    {{ csrf_field() }}

                    @include('pages.member.partials.forms')
                    <div class="form-group">
                        <div class="col-xs-1 col-xs-offset-2">
                            <button type="submit" class="btn btn-primary" placeholder"Submit"><i class="glyphicon glyphicon-check"></i> Save</button>
                        </div>
                        <div class="col-xs-1">
                            <a href="{{ route('member.index') }}" title="Cancel" class="btn btn-primary cancel_btn">
                            <i class="glyphicon glyphicon-remove-circle"></i> Cancel
                            </a>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{ asset('vendor/dhtmlxSuite/codebase/dhtmlx.js') }}"></script>
<script src="{{ asset('js/helpers.js') }}"></script>
<script src="{{ asset('js/ajax-interceptor.js') }}"></script>

<script src="{{ asset('vendor/adminlte/plugins/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('.datepicker').datepicker({
			format: 'yyyy-mm-dd'
        });
    });

</script>
@endsection