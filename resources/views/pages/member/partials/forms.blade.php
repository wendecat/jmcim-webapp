<div class="form-group">
        <label for="type" class="col-xs-2 control-label">Type:</label>
        <div class="col-xs-5">
            {{Form::select('type_id', $type ,  null ,['class' => 'form-control'])}}
        </div>
</div>
<div class="form-group">
    <label for="name" class="col-xs-2 control-label">Name:</label>
    <div class="col-xs-5">
    <input type="text" name="name" class="form-control" value="{{ $member->name }}" placeholder="name">
    </div>
</div>
<div class="form-group">
        <label for="gender" class="col-xs-2 control-label">Gender:</label>
        <div class="col-xs-5">
            {{ Form::select('gender', ['----' => '----','male' => 'male', 'female' => 'female'] ,  null ,['class' => 'form-control']) }}
        </div>
</div>
<div class="form-group">
    <label for="date_saved" class="col-xs-2 control-label">Date Saved:</label>
    <div class="col-xs-5">
        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            <input type="text" class="form-control datepicker" name="date_saved"   @if($member->date_saved == null) value="" @else value="{{ date("Y-m-d", strtotime($member->date_saved) ) }}" @endif>
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="address" class="col-xs-2 control-label">Address:</label>
    <div class="col-xs-5">
    <input type="text" name="address" class="form-control" value="{{ $member->address }}" placeholder="address">
    </div>
</div>
<div class="form-group">
    <label for="contact" class="col-xs-2 control-label">Contact:</label>
    <div class="col-xs-5">
    <input type="text" name="contact" class="form-control" value="{{ $member->contact }}" placeholder="contact">
    </div>
</div>
<div class="form-group">
    <label for="email" class="col-xs-2 control-label">Email:</label>
    <div class="col-xs-5">
    <input type="text" name="email" class="form-control" value="{{ $member->email }}" placeholder="email">
    </div>
</div>
<div class="form-group">
    <label for="birth_date" class="col-xs-2 control-label">Birth Date:</label>
    <div class="col-xs-5">
        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            <input type="text" class="form-control datepicker" name="birth_date"   @if($member->birth_date == null) value="" @else value="{{ date("Y-m-d", strtotime($member->birth_date) ) }}" @endif>
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="occupation" class="col-xs-2 control-label">Occupation:</label>
    <div class="col-xs-5">
    <input type="text" name="occupation" class="form-control" value="{{ $member->occupation }}" placeholder="occupation">
    </div>
</div>
<div class="form-group">
    <label for="special_skill" class="col-xs-2 control-label">Special Skill:</label>
    <div class="col-xs-5">
    <input type="text" name="special_skill" class="form-control" value="{{ $member->special_skill }}" placeholder="special_skill">
    </div>
</div>
<div class="form-group">
    <label for="vices" class="col-xs-2 control-label">Vices:</label>
    <div class="col-xs-5">
    <input type="text" name="vices" class="form-control" value="{{ $member->vices }}" placeholder="vices">
    </div>
</div>
<div class="form-group">
    <label for="diseases" class="col-xs-2 control-label">Diseases:</label>
    <div class="col-xs-5">
    <input type="text" name="diseases" class="form-control" value="{{ $member->diseases }}" placeholder="diseases">
    </div>
</div>