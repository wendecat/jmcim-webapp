import Vue from 'vue';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import { MemberIndex } from './components/member/index';

Vue.use(Buefy);


const app = new Vue(MemberIndex)
app.$mount('#app')
        