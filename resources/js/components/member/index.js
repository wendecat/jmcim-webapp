/**
 *  Member Index File
 */

import axios from 'axios';

export const MemberIndex =  {
    name: "MemberIndex",
    data() {
        return {
            data : [],
            isEmpty: false,
            isBordered: false,
            isStriped: false,
            isNarrowed: false,
            isHoverable: true,
            isFocusable: false,
            isLoading: false,
            hasMobileCards: true,
            
            currentPage: 1,
            perPage: 10,
            total: 0,
            order: 'default',
            size: 'default',
            isSimple: true,
            isRounded: true,
            
            search_query: ''
        }
    },
    computed: {
        filter: function() {
          var name_re = new RegExp(this.search_query.name, 'i')
          var data = [];
          var i=0;
          for (i=0; i < this.data.length; i++) {
           
            if (!this.data[i].name.match(name_re)) {
                this.searchData();
            } else {
                data.push(this.data[i]);
            } 

          }
          return data
        }
    },
    mounted: function(){
        this.fetchData();
    },
    methods: {
        fetchData: function() {
            axios.get('/api/member/paginate?per_page=' + this.perPage + '&page=' + this.currentPage)
                .then(function (response) {
                    // handle success
                    this.total = response.data['total'];
                    this.data = response.data['data'];
                }.bind(this))
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }, 

        searchData: function(){
            axios.get('/api/member/search?keyword=' + this.search_query)
                .then(function (response) {
                    // handle success
                    this.total = response.data['total'];
                    this.data = response.data['data'];
                }.bind(this))
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }
    },

    watch: {
        currentPage: function () {
            this.fetchData();
        },

        perPage: function () {
            this.fetchData();
        },

        search_query : function(){
            if(this.search_query == ""){
                this.fetchData(); console.log("search_query blank");
            }else {
                this.searchData();
            }
        }
      }
}