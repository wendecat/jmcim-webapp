import VuetableFieldSequence from 'vuetable-2/src/components/VuetableFieldSequence.vue'

export default [
  {
    name: VuetableFieldSequence,
    title: "No.",
    width: "5%"
  },
  {
    name: "type.name",
    title: '<i class="grey user outline icon"></i>Type',
    width: "15%",
    sortField: "type.name"
  },
  {
    name: "title",
    title: '<i class="grey user outline icon"></i>Title',
    width: "10%",
    sortField: "title"
  },
  {
    name: "name",
    title: '<i class="grey mail outline icon"></i>Name',
    width: "50%",
    sortField: "name"
  },
  // {
  //   name: "gender",
  //   title: '<i class="grey sitemap icon"></i>Gender',
  //   width: "15%",
  //   sortField: "gender"
  // },
  // {
  //   name: "address",
  //   title: '<i class="grey heterosexual icon"></i>Address',
  //   width: "15%",
  //   sortField: "address"
  // },
  // {
  //   name: "contact",
  //   title: '<i class="grey heterosexual icon"></i>Contact',
  //   width: "15%",
  //   sortField: "contact"
  // },
  // {
  //   name: "email",
  //   title: '<i class="grey heterosexual icon"></i>Email',
  //   width: "15%",
  //   sortField: "email"
  // },
  // {
  //   name: "birth_date",
  //   title: '<i class="grey heterosexual icon"></i>Birth Date',
  //   width: "15%",
  //   sortField: "birth_date"
  // },
  // {
  //   name: "occupation",
  //   title: '<i class="grey heterosexual icon"></i>Occupation',
  //   width: "15%",
  //   sortField: "occupation"
  // },
  'actions'
];
