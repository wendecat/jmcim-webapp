/**
 * Global Ajax interceptor
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    statusCode: {
        /**
         * Global interceptor for redirecting unauthenticated ajax request.
         */
        401: function (err) {
            var message = (err.responseJSON && err.responseJSON['message']) || 'Please login again to continue.';

            dhtmlx.alert({
                title: 'Unauthorized Request',
                type: "alert-error",
                text: message,
                callback: function (result) {
                    window.location.replace(window.baseUrl);
                }
            });
        },

        /**
         * Global interceptor for handling http code 500 on ajax request.
         */
        500: function (err) {
            var message = (err.responseJSON && err.responseJSON['message']) || 'Something went wrong on the server. Please try again or contact the system administrator.';
            dhtmlx.alert({
                title: 'Internal Server Error',
                type: "alert-error",
                text: message
            });
        },

        /**
         * Global interceptor for handling http code 400 on ajax request.
         */
        400: function (err) {
            var message = (err.responseJSON && err.responseJSON['message']) || 'There was a problem with your request. Please try again or contact the system administrator.';
            dhtmlx.alert({
                title: 'Bad Request',
                type: "alert-warning",
                text: message
            });
        },

        /**
         * Global interceptor for handling http code 404 on ajax request.
         */
        404: function (err) {
            var message = (err.responseJSON && err.responseJSON['message']) || 'There was a problem with your request. Please try again or contact the system administrator.';
            dhtmlx.alert({
                title: '404 Request',
                type: "alert-warning",
                text: message
            });
        }
    }
});


