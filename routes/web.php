<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'web'], function () {

            Auth::routes();

            Route::group([
                'prefix' => '',
                'middleware' => ['auth']
                // 'middleware' => ['role:telco.*'],
            ], function() {
                    Route::resource('member', 'MemberController', [
                        'only' => ['index','create','store','edit','update','destroy']
                    ]);
                    Route::resource('user', 'UserController', [
                        'only' => ['index','create','store','edit','update','destroy']
                    ]);
                    Route::resource('testimony', 'TestimonyController');
                }
            );
        }
    );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
