<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/hello', function(){
    return 'Hello World!';
});
Route::post('/user', 'UserController@store');

Route::get('member/paginate', 'ApiMemberController@paginate')->name('member.paginate');
Route::get('member/search', 'ApiMemberController@search')->name('member.search');



