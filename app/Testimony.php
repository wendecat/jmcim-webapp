<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    protected $fillable =[
        'member_id',
        'description'
    ];

    public function Member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
