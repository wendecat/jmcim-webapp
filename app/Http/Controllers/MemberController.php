<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

use App\Member;
use App\Type;

class MemberController extends Controller
{

    /**
     * @param Member $member
     *
     */

    protected $member;

    public function __construct(Member $member){
        $this->member = $member;

        // $this->middleware('role:telco.admin', ['only' => ['create', 'store','edit','update','destroy']]);
        // $this->middleware('role:telco.admin|telco.user', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     * 
     */
    public function index(Request $request)
	{   
        if($request->ajax()) {
            $member = $this->member->With('Type')->get();
            return Datatables::of($member)->make(true);
        }
        return view('pages.member.index');
    }

/**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member){
        $type = Type::pluck('name','id')->reverse()->put('', '-----')->reverse();
        return view('pages.member.edit', compact('member', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member){
        $this->validate($request,[
            'type_id' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'date_saved' => 'date',
            'address' => '',
            'contact' => '',
            'email' => 'email',
            'birth_date' => 'date',
            'occupation' => '',
            'special_skill' => '',
            'vices' => '',
            'diseases' => ''
        ]);
        $member->update($request->only([
            'type_id',
            'name',
            'gender',
            'date_saved',
            'address',
            'contact',
            'email',
            'birth_date',
            'occupation',
            'special_skill',
            'vices',
            'diseases'
        ]) );
        return view('pages.member.index');
    }

    /*
    *  open the form to create new resource
    *
    *
    */
    public function create(){
        $member = new Member();
        $type = Type::pluck('name','id')->reverse()->put('', '-----')->reverse();
        return view('pages.member.create', compact('member','type') );
    }

    /*
    *  store the newly created resource
    *
    *
    */
    public function store(Request $request, User $user){
        $this->validate($request,[
            'type_id' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'date_saved' => 'date',
            'address' => '',
            'contact' => '',
            'email' => 'email',
            'birth_date' => 'date',
            'occupation' => '',
            'special_skill' => '',
            'vices' => '',
            'diseases' => ''
        ]);

        $user->create($request->only([
            'type_id',
            'name',
            'gender',
            'date_saved',
            'address',
            'contact',
            'email',
            'birth_date',
            'occupation',
            'special_skill',
            'vices',
            'diseases'
        ]) );
        return view('pages.member.index');
    }

    /*
    *  delete the selected resource
    *
    *
    */
    public function destroy(Member $member)
    {
        $member->delete();
        return response()->json([
			"success" => "true",
			"message" => "Member has been has been deleted.",
		]);
    }
}
