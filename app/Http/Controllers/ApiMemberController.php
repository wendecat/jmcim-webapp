<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Member;
use App\Type;
use App\Vice;
use App\Disease;

class ApiMemberController extends Controller
{

    /**
     * @param Member $member
     *
     */

    protected $member;

    public function __construct(Member $member){
        $this->member = $member;

    }
    
    /*
    * data pagination
    */
    public function paginate()
	{

        $per_page = Input::get('per_page') ?: 10;
        $members = Member::with('Type')->paginate($per_page);
        return $members; 
    }

    /*
    * search data
    */
    public function search()
	{
        $keyword = Input::get('keyword');
        $per_page = Input::get('per_page') ?: 10;
        $members = Member::with('Type')->where('name', 'like', $keyword . '%')->paginate($per_page);
        return $members; 
    }
}
