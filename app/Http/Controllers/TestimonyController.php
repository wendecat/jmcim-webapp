<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modal;

use App\Testimony;

class TestimonyController extends Controller
{
    /**
     * @param Testimony $testimony
     *
     */

    protected $testimony;

    public function __construct(Testimony $testimony){
        $this->testimony = $testimony;

        // $this->middleware('role:telco.admin', ['only' => ['create', 'store','edit','update','destroy']]);
        // $this->middleware('role:telco.admin|telco.user', ['only' => ['index']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $testimony = $this->testimony->get();
            return Datatables::of($testimony)->make(true);
        }
        return view('pages.testimony.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testimony = $this->testimony->with('Member')->find($id);
        return Modal::make('pages.testimony.show', compact('testimony') );
        // return $testimony;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
