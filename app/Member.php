<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'type_id',
        'title',
        'name',
        'gender',
        'date_saved',
        'address',
        'contact',
        'email',
        'birth_date',
        'occupation',
        'special_skill',
        'vices',
        'diseases'
    ];

    public function Type(){
        return $this->belongsTo(Type::class, 'type_id');
    }
}
