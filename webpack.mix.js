const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts( [
        'resources/js/vendor/axios.min.js',
        // 'resources/js/vendor/buefy.min.js',
        'resources/js/vendor/cleave.min.js',
        'resources/js/vendor/lodash.min.js',
        'resources/js/vendor/Sortable.min.js',
        'resources/js/vendor/vee-validate.min.js',
        // 'resources/js/vendor/vue.min.js',
        'resources/js/vendor/vue-resource@1.5.1.js',
    ],  'public/js/vendor.js');

mix.styles([
    // 'resources/css/vendor/buefy.min.css',
    'resources/css/vendor/materialdesignicons.min.css',
    // 'resources/css/vendor/font-awesome.min.css'
    'resources/css/vendor/all.css'
], 'public/css/vendor.css');

mix.js('resources/js/app.js', 'public/js')
   .sourceMaps();