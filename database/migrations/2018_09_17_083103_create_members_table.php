<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_id');
            $table->string('title')->nullable();
            $table->string('name');
            $table->string('gender');
            $table->date('date_saved');
            $table->text('address');
            $table->string('contact')->nullable();
            $table->string('email')->nullable()->unique();
            $table->date('birth_date');
            $table->string('occupation')->nullable();
            $table->text('special_skill');
            $table->text('vices');
            $table->text('diseases');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
