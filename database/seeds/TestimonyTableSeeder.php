<?php

use Illuminate\Database\Seeder;

class TestimonyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Testimony::class, 50)->create();
    }
}
