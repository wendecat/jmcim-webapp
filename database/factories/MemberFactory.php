<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Member::class, function (Faker $faker) {

    $gender = $faker->randomElements(['male', 'female']);
    return [
            'type_id' => $faker->numberBetween($min = 1, $max = 4),
            'title' => (string)$faker->title($gender),
            'name' => (string)$faker->name($gender),
            'gender' => implode('', $gender),
            'date_saved' => $faker->date($format = 'Y-m-d', $max = 'now'),
            'address' => (string)$faker->address,
            'contact' => $faker->randomNumber(7, false),
            'email' =>  $faker->unique()->safeEmail,
            'birth_date' => $faker->date($format = 'Y-m-d', $max = '-30 years'),
            'occupation' => (string)$faker->jobTitle,
            'special_skill' => (string)$faker->word,
            'vices'     => (string)$faker->words($nb=3, $asText=true),
            'diseases'  => (string)$faker->words($nb=3, $asText=true)
    ];
});

// insert into `members` (`type_id`, `title`, `name`, `gender`, `date_saved`, `address`, `contact`, `email`, `birth_date`, `occupation`, `special_skill`, `vices`, `diseases`, `updated_at`, `created_at`) 
// values (4, 'Prof.', 'Colby Parisian IV', 'male', '2008-04-26', 'Philippines', '5503864', 'jeanette.torp@example.net', '1975-04-01', 'Computer Science Teacher', 'facilis', 'nihil, voluptatem,dolorem', 'voluptatum, magnam,necessitatibus', NOW(), NOW())